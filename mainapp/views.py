from django.shortcuts import render

def index(request):
    return render(request, 'mainapp/homePage.html')

def contact(request):
    return render(request, 'mainapp/basic.html', {'values': ['Если у вас остались вопросы, то задавайте их мне по телефону', '(066) 986-41-47']})

